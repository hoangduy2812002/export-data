from flask import Flask
from flask_cors import CORS
from export import Export

app = Flask(__name__)
CORS(app)
@app.route('/response')
def main():
    text_as_string = open('cn.txt', 'r').read() + open('mc.txt', 'r').read() +open('blockchain.txt', 'r').read()
    return text_as_string

if __name__ == '__main__':
   app.run()