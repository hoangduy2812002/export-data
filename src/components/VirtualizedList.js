import React, { useState, useEffect, useRef } from 'react';
import Grid from '@mui/material/Grid';
import { Box, TextField, Typography } from '@mui/material';
import Stack from '@mui/material/Stack';
import LinearProgress from '@mui/material/LinearProgress';
import CircularProgress from '@mui/material/CircularProgress';




const LargeList = ({ data, listSames, value, setValue }) => {
    const [visibleData, setVisibleData] = useState([]);
    const itemsPerPage = 100;
    const containerRef = useRef(null);

    useEffect(() => {
        // const listDataConvert=[...data]
        // setVisibleData(listDataConvert.sort(compareDataLength).slice(0, itemsPerPage));
        setVisibleData(data.slice(0, itemsPerPage))
    }, [data]);
     // Hàm so sánh, sắp xếp theo số lượng phần tử trong trường "data"
     function compareDataLength(a, b) {
        return b.value.length - a.value.length;
    }

    const handleScroll = () => {
        const container = containerRef.current;
        if (
            container.scrollTop + container.clientHeight >= container.scrollHeight - 20 &&
            visibleData.length < data.length &&
            !true
        ) {
            const nextPageItems = data.slice(visibleData.length, visibleData.length + itemsPerPage);
            setVisibleData((prevVisibleData) => [...prevVisibleData, ...nextPageItems]);
        }
    };

    return (
        <>
            <Grid container>
                <Grid item xs={12} sm={12} md={12} sx={{ mt: '1vh' }}>
                    <Box sx={{ margin: 'auto', textAlign: 'center' }}>
                        <TextField
                            id="standard-basic"
                            label="Số cầu khớp" variant="standard"
                            sx={{ width: '80px' }}
                            value={value || ''}
                            onChange={(e) => { setValue(e.target.value) }}
                        />
                    </Box>
                </Grid>
                <Grid item xs={12} sm={12} md={12} sx={{ mt: '2vh' }}>
                    {listSames?.length !==0 && listSames &&
                        <Grid item xs={12} sm={12} md={12}>
                            <Grid container>
                                {listSames?.map((item, index) => {
                                    return <Grid key={index} item xs={12} sm={6} md={4} sx={{ margin: 'auto' }}>
                                        <p></p>
                                        <Box sx={{ boxShadow: 2, textAlign: 'center', p: 1, color: 'black', background: '#f0f3fa', fontWeight: 700, borderRadius: 2, width: '96%', margin: 'auto', border: '1px solid #f5222d' }}>

                                            <Typography sx={{ textAlign: 'center', fontWeight: 600, fontSize: 20, }}>{item?.item1?.name} -
                                                Tổng ( {item?.item1?.found.reduce((acc, curr) => acc + curr, 0) + item?.item1?.part2?.reduce((acc, curr) => acc + curr, 0)} )</Typography>

                                            <Stack direction="row" spacing={0.5}
                                                alignItems="center"
                                                useFlexGap flexWrap="wrap">
                                                {item?.item1?.part1.map((value, j) => {
                                                    return <Box key={j} sx={{ color: 'white', background: '#8c8c8c', boxShadow: 1, paddingLeft: 1, paddingRight: 1, borderRadius: 50 }} >{value}</Box>
                                                })}
                                                {item?.item1?.found.map((value, j) => {
                                                    return <Box sx={{ color: 'white', background: '#fa8c16', boxShadow: 1, paddingLeft: 1, paddingRight: 1, borderRadius: 50 }} key={j} >{value}</Box>
                                                })}
                                                {item?.item1?.part2?.map((value, j) => {
                                                    return <Box sx={{ color: 'white', background: '#7cb305', boxShadow: 1, paddingLeft: 1, paddingRight: 1, borderRadius: 50 }} key={j} >{value}</Box>
                                                })}
                                            </Stack>

                                            <Typography sx={{ textAlign: 'center', fontWeight: 600, fontSize: 20, }}>{item?.item2?.name} -
                                                Tổng ( {item?.item2?.found.reduce((acc, curr) => acc + curr, 0) + item?.item2?.part2?.slice(0, 1).reduce((acc, curr) => acc + curr, 0)} )</Typography>
                                            <Stack direction="row" spacing={0.5}
                                                alignItems="center"
                                                useFlexGap flexWrap="wrap" >
                                                {item?.item2?.part1?.map((value, j) => {
                                                    return <Box key={j} sx={{ color: 'white', background: '#8c8c8c', boxShadow: 1, paddingLeft: 1, paddingRight: 1, borderRadius: 50 }}>{value}</Box>
                                                })}
                                                {item?.item2?.found?.map((value, j) => {
                                                    return <Box sx={{ color: 'white', background: '#fa8c16', boxShadow: 1, paddingLeft: 1, paddingRight: 1, borderRadius: 50 }} key={j} >{value}</Box>
                                                })}
                                                {item?.item2?.part2?.map((value, j) => {
                                                    return <Box key={j} sx={{ color: 'white', background: j === 0 ? '#7cb305' : '#8c8c8c', boxShadow: 1, paddingLeft: 1, paddingRight: 1, borderRadius: 50 }} >{value}</Box>
                                                })}
                                            </Stack>
                                        </Box>
                                    </Grid>
                                })}
                                <Grid item xs={12} sm={12} md={12} sx={{ mt: 2, mb: 1 }}>
                                    <LinearProgress color="success" />
                                </Grid>
                            </Grid>
                        </Grid>

                    }
                    {visibleData && <Grid item xs={12} sm={12} md={12}>
                        <Typography sx={{ textAlign: 'center', fontWeight: 600, fontSize: 20, mb: '1vh' }}>Tổng: {data?.length} bàn</Typography>
                    </Grid>}
                    {visibleData &&
                        <Grid container ref={containerRef} onScroll={handleScroll} sx={{ height: '70vh', overflow: 'auto' }} >
                            {visibleData?.map((item, index) => (
                                <Grid key={index} item xs={12} sm={6} md={4} sx={{ mt: 1, }}>
                                    <Box sx={{ width: '90%', margin: 'auto', border: '1px solid #87e8de', background: '#f0f5ff', boxShadow: 3, borderRadius: 1, p: 1 }}>
                                        <Typography sx={{ textAlign: 'center', fontWeight: 600, fontSize: 20, }}>{item?.name}</Typography>
                                        <Stack spacing={0.5} justifyContent="center"
                                            alignItems="center" direction="row"
                                            useFlexGap flexWrap="wrap"
                                        >
                                            {item?.value?.map((element, j) => {
                                                return <Box key={j} sx={{ background: '#8c8c8c', color: "#fff", boxShadow: 1, paddingLeft: 1, paddingRight: 1, borderRadius: 50, }}>{element}</Box>
                                            })}
                                            {item?.value?.length === 0 &&
                                                <Box>
                                                    <Typography sx={{ textAlign: 'center' }}><CircularProgress color="success" size={25} /></Typography>
                                                    <Typography sx={{ textAlign: 'center', fontWeight: 600 }}>( Đang chờ lấy bài mới )</Typography>
                                                </Box>
                                            }
                                        </Stack>
                                    </Box>
                                </Grid>
                            ))}
                            {/* {isLoading && <div>Loading...</div>} */}
                        </Grid>}
                </Grid>
            </Grid>
        </>
    );
};

export default LargeList;