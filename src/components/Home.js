import { useEffect, useState } from 'react';
import axios from 'axios';
import LargeList from './VirtualizedList';

export default function Home() {
    const [htmlContent, setHtmlContent] = useState();
    const [value, setValue] = useState(20);
    const [listOld, setListOld] = useState();
    const [listSames, setListSame] = useState();

    useEffect(() => {
        fetchData();
        let timeOut = setInterval(() => {
            fetchData();
        }, 5001)
        return () => clearInterval(timeOut)
    }, []);

    // useEffect(() => {
    //     fetchDataTest();
    //     let timeOut = setInterval(() => {
    //         fetchDataTest();
    //     }, 2000)
    //     return () => clearInterval(timeOut)
    //     // dispatch(setListData(fakeData))

    // }, []);

    const fetchData = async () => {
        try {
            const response = await axios.get('http://127.0.0.1:5000/response', {
            });
            setHtmlContent(response.data)
        } catch (error) {
            console.error(error);
        }
    };


    const fetchDataTest = async () => {
        try {
            const response = await axios.get('http://localhost:3001/api/data', {});
            setHtmlContent(response.data)
            // setHtmlContent(s)

        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        if (htmlContent) {
            readFileOld()
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [htmlContent])

    const readFileOld = async () => {
        let dataFile = await readFile(htmlContent)
        let listResult = listOld;
        const temporary = [];

        if (!listResult) {
            console.log('undefined')
            for (let i = 0; i < dataFile.length; i++) {
                const element = dataFile[i];
                const newArray = element.data.join('').split('').map(Number)
                let numberCount = 0; // Bắt đầu với 1 vì ta đã có ít nhất một phần tử đầu tiên trong mảng
                let arraySplit = [];
                // bo di cac so khong giong nhau o lan dau tien neu data lon hon 24
                if (element.data.length > 24) {
                    for (let i = 0; i < newArray.length; i++) {
                        if (newArray[i] === newArray[0] || newArray[i] === 3) {
                            numberCount++;
                        } else {
                            break; // Dừng vòng lặp khi tìm thấy phần tử khác
                        }
                    }
                    arraySplit = newArray.slice(numberCount)
                } else {
                    arraySplit = newArray
                }


                const filteredArray = arraySplit.filter((item) => item !== 3);
                const resultNumber = []
                let count = 1;
                for (let i = 0; i < filteredArray.length; i++) {
                    if (filteredArray[i] !== 0) {
                        if (filteredArray[i] === filteredArray[i + 1]) {
                            count++;
                        } else {
                            resultNumber.push(count);
                            count = 1;
                        }
                    }
                }
                dataFile[i].textPast = arraySplit.join('');

                dataFile[i].value = resultNumber;
            }
            temporary.push(...dataFile)
        } else {
            const idSet = new Set(listResult.map(item => item.id));
            for (const item of dataFile) {
                if (!idSet.has(item.id)) {
                    console.log("========= ADD Data =========")
                    const arraySplit = item.data.join('').split('').map(Number)
                    const filteredArray = arraySplit.filter((item) => item !== 3);
                    const resultNumber = []
                    let count = 1;
                    for (let i = 0; i < filteredArray.length; i++) {
                        if (filteredArray[i] !== 0) {
                            if (filteredArray[i] === filteredArray[i + 1]) {
                                count++;
                            } else {
                                resultNumber.push(count);
                                count = 1;
                            }
                        }
                    }
                    item.textPast = resultNumber.join('');
                    item.value = resultNumber;
                    listResult.push(item)
                }

            }
            temporary.push(...listResult)
        }


        for (let index = 0; index < temporary.length; index++) {
            const element = temporary[index];
            const foundObject = dataFile.find(item => item.id === element.id);
            if (foundObject) {
                if (foundObject.data.length === 0) {
                    console.log('====== RESET ======', element)
                    temporary[index].textPast = '';
                    temporary[index].data = [];
                    temporary[index].value = [];
                } else {
                    if (element.data.length !== foundObject.data.length) {
                        let lastElement = foundObject.data[foundObject.data.length - 1];
                        console.log("======= CREATE =========", element)
                        temporary[index].textPast = temporary[index].textPast + '' + lastElement;
                        temporary[index].data = foundObject.data;

                        if (lastElement !== 3) {
                            const listDataConvert = temporary[index].textPast.split('').map(Number)
                            const filteredArray = listDataConvert.filter((item) => item !== 3);
                            let count = 1;
                            const resultNumber = []
                            for (let i = 0; i < filteredArray.length; i++) {
                                if (filteredArray[i] !== 0) {
                                    if (filteredArray[i] === filteredArray[i + 1]) {
                                        count++;
                                    } else {
                                        resultNumber.push(count);
                                        count = 1;
                                    }
                                }
                            }
                            temporary[index].value = resultNumber;
                        }
                    }
                }
            }
        }

        const response = [...temporary]
        console.log(response)
        setListOld([...response])
    }

    useEffect(() => {
        if (listOld) {
            getAllDataSames(listOld)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [listOld])



    const getAllDataSames = (listData) => {
        let listTotalData = []
        //========kiểm tra trong một mảng có tồn tại các cặp chỉ số liền kề mà tổng bằng với số cầu khớp
        for (let index = 0; index < listData.length; index++) {
            const element = listData[index];
            let check = checkSumGreater(element?.value, value - 1);
            if (check) {
                listTotalData.push(element)
            }
        }
        // ========So sanh chuoi======
        const listFindCommonArray = []
        for (let index = 0; index < listTotalData.length; index++) {
            const element = listTotalData[index];

            for (let jk = 0; jk < listTotalData.length; jk++) {
                const element2 = listTotalData[jk];
                if (index === jk) {

                } else {
                    const listFind = findCommonArrays(element.value, element2.value);
                    if (listFind.length !== 0) {
                        // console.log(listFind)
                        let data = {
                            found: listFind[0],
                            item1: element,
                            item2: element2
                        }
                        listFindCommonArray.push(data)
                    }
                }

            }
        }
        // console.log(listFindCommonArray)

        const result = []
        for (let index = 0; index < listFindCommonArray.length; index++) {
            const element = listFindCommonArray[index];

            const splitResult = splitArray(element.item1.value, element.found);
            splitResult.id = element.item1.id
            splitResult.name = element.item1.name
            element.item1 = splitResult

            const splitResult2 = splitArray(element.item2.value, element.found);
            splitResult2.id = element.item2.id
            splitResult2.name = element.item2.name
            element.item2 = splitResult2
            if (element.item2.part2.length > 0) {
                let checkItem1 = element.item1.part2.slice(element.item1.part2.length - 1)[0]
                let checkItem2 = element.item2.part2.slice(0, 1)[0]
                if (checkItem1 > checkItem2 - 1) {

                } else {
                    // check found phai du 20
                    if (element.item1.part2.length < 2 && (element.item1.found.reduce((acc, curr) => acc + curr, 0) + element.item1.part2.reduce((acc, curr) => acc + curr, 0)) > value - 1) {
                        result.push(element);
                    }
                }
            }

        }
        // xoa cac id trung nhau trong cung object
        let uniqueData = result.filter(item => item.item1.id !== item.item2.id);
        console.log('uniqueData---===============> ', uniqueData)
        if (uniqueData.length > 0) {
            postData(uniqueData);
        }
        setListSame(uniqueData);
    }

    const postData = async (content) => {

        try {
            await axios.post('http://127.0.0.1:5000/data', {
                ...content
            });
            // console.log(response);
        } catch (error) {
            console.error(error);
        }
    };

    function splitArray(arr1, arr2) {
        const result = {
            part1: [],
            found: [],
            part2: []
        };
        let currentIndex = 0;
        while (currentIndex < arr1.length) {
            if (
                arr1[currentIndex] === arr2[0] &&
                arr1.slice(currentIndex, currentIndex + arr2.length).join(',') === arr2.join(',')
            ) {
                result.found = arr1.slice(currentIndex, currentIndex + arr2.length);
                currentIndex += arr2.length;
            } else if (result.found.length === 0) {
                result.part1.push(arr1[currentIndex]);
                currentIndex++;
            } else {
                result.part2.push(arr1[currentIndex]);
                currentIndex++;
            }
        }

        return result;
    }

    function findCommonArrays(arr1, arr2) {
        const result = [];
        if (arr1.length > arr2.length - 1) {
            return result;
        } else {
            for (let i = 0; i < arr1.length; i++) {
                const element = arr1[i];
                const matchingIndexes = [];

                for (let j = 0; j < arr2.length; j++) {
                    if (arr2[j] === element) {
                        matchingIndexes.push(j);
                    }
                }

                for (let k = 0; k < matchingIndexes.length; k++) {
                    const index = matchingIndexes[k];
                    const commonArray = [element];
                    for (let l = 1; l < arr1.length - i && l < arr2.length - index; l++) {
                        if (arr1[i + l] === arr2[index + l]) {
                            commonArray.push(arr1[i + l]);
                        } else {
                            break;
                        }
                    }
                    if (commonArray.length > 1) {
                        const isTargetSum = isSumEqualToTarget(commonArray, value - 1);
                        if (isTargetSum) {
                            result.push(commonArray);
                        } else {
                            //====check lan 2 =====
                            const newArray = [...commonArray];
                            newArray.push(arr1.slice(arr1.length - 1)[0]);
                            if (newArray.length > 1) {
                                const isTargetSum = isSumEqualToTarget(newArray, value - 1);
                                if (isTargetSum) {
                                    result.push(newArray.slice(0, newArray.length - 1));
                                }
                            }
                        }
                    }


                }
            }
            return result;
        }


    }

    function isSumEqualToTarget(arr, target) {
        let currentSum = 0;

        for (let i = 0; i < arr.length; i++) {
            currentSum += arr[i];

            if (currentSum > target) {
                return true;
            }
        }

        return false;
    }

    function checkSumGreater(array, number) {
        let sum = 0;

        for (let i = 0; i < array?.length; i++) {
            sum += array[i];
        }

        return sum > number;
    }




    const readFile = async (data) => {
        try {
            const parser = new DOMParser();
            const htmlDocument = parser.parseFromString(data, 'text/html');

            const game_box = htmlDocument.getElementsByClassName("game_box");

            const listDataConvert = []
            for (let index = 0; index < game_box.length; index++) {
                const rows = game_box[index].children[1].children[1].childNodes[2].rows;
                //==================== Lay Id phong ======================
                const id = game_box[index].id;
                //==================== Lay ten phong ======================
                //tach khoang trang xuong dong
                let text = game_box[index].children[0].innerText.trim().replace(/\s+/g, ' ')
                // Tìm vị trí của khoảng trắng cuối cùng
                const lastSpaceIndex = text.lastIndexOf(' ');
                // Xoá các kí tự sau khoảng trắng cuối cùng
                if (lastSpaceIndex !== -1) {
                    text = text.substring(0, lastSpaceIndex + 1);
                }
                // Xóa bỏ "\n10" và tách chữ cuối cùng ra khỏi chuỗi
                const convertText = text.split('\n')[0].trim();
                // Lấy chữ cuối cùng của chuỗi
                const lastChar = convertText.slice(-1);
                // Thêm khoảng trắng trước chữ cuối cùng
                const modifiedString = convertText.slice(0, -1).concat(" ", lastChar);
                // =====================================================================
                let listRow = [];
                listRow.push(...rows)
                let listNumber = []

                for (let index = 0; index < listRow.length; index++) {
                    let result = listRow.map(obj => obj.children[index]);

                    for (let jk = 0; jk < result.length; jk++) {
                        const element = result[jk].innerText.replace(/\n/g, '');
                        if (element === "P") {
                            listNumber.push(2);
                        }
                        if (element === "T") {
                            listNumber.push(3);

                        }
                        if (element === "B") {
                            listNumber.push(1);
                        }
                    }
                }

                listDataConvert.push({ id: id, name: modifiedString, data: listNumber })
            }
            return listDataConvert;

        } catch (error) {
            console.error('Error:', error);
        }
    };

    return <>
        {listOld && <LargeList data={listOld} listSames={listSames} value={value} setValue={setValue} />}
    </>

}