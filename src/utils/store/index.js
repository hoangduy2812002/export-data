import { createStore } from 'redux';
import reducer from './reducer';
import { composeWithDevTools } from 'redux-devtools-extension';
// ==============================|| REDUX - MAIN STORE ||============================== //
const composeEnhancers = composeWithDevTools();
const store = createStore(reducer,composeEnhancers);
const persister = 'Free';

export { store, persister };
