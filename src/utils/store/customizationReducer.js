
export const initialState = {
};

// ==============================|| CUSTOMIZATION REDUCER ||============================== //

const customizationReducer = (state = initialState, action) => {
    // console.log('action',action)
    switch (action.type) {
        case 'setListData':
            return {
                ...state,
                listData: action.payload
            }
        default:
            return state;
    }
};



export { customizationReducer };
