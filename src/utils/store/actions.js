export const setListData = (data) => {
    return {
        type: 'setListData',
        payload: data
    }
}